#include <string>
#include <iostream>
#include "Category.h"
#include <set>

#include "Item.h"

Category::Category(const std::string name) : m_name(name), m_quantity(0) 
{
}

Category::~Category() {
	if ( !m_items.empty() ) {
		std::set<Item*>::iterator it = m_items.begin();

		while ( it != m_items.end() ) {
			delete *it;
			it++;
		}
	}

	std::cout << "Category \"" << m_name << "\" is deleted!\n";
}

std::string Category::getName() const {
	return m_name;
}

long long Category::getQuantity() const {
	return m_quantity;
}

void Category::addItem(Item* item) {
	m_items.insert(item);
	m_quantity += 1;
}

void Category::deleteItem(Item* item) {
	m_items.erase(item);
}

void Category::showItems() const {
	if ( !m_items.empty() ) {
		auto it = m_items.begin();

		while ( it != m_items.end() ) {
			std::cout << *it << std::endl;
			it++;
		}
	}
}

std::ostream& operator<<(std::ostream& out, const Category* category) {
	out << category->getName() << ": " << category->getQuantity() << " items";

	return out;
}