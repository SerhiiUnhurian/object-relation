#include "Order.h"
#include "Item.h"
#include "Customer.h"

Order::Order(Customer* owner, Item* item) : m_owner(owner) 
{
	m_id = ++s_idGenerator;

	addItem(item);

	m_owner->addOrder(this);
}

Order::~Order() {
	m_owner->deleteOrder(this);
	
	std::cout << "Order #" << m_id << " is deleted!\n";
}

long Order::getID() const {
	return m_id;
}

Customer* Order::getOwner() const {
	return m_owner;
}

void Order::getItems() const {
	auto it = m_items.begin();

	while ( it != m_items.end() ) {
		std::cout << it->first->getName() << ": " << it->second << " units.\n";
		it++;
	}
}

void Order::addItem(Item* item, int quantity) {
	if ( m_items.count(item) == 1 ) {
		m_items[item] += quantity;
	} else {
		m_items.insert(std::pair<Item*,int>(item, quantity));
		item->addOrder(this);
	}
}

void Order::reduceItem(Item* item) {
	if ( m_items.count(item) == 1 ) {
		m_items[item] -= 1;

		if (m_items[item] <= 0) 
			m_items.erase(item);

		if (m_items.empty() ) {
			delete this;
		}
	} else {
		return;
	}	
}

void Order::deleteItem(Item* item) {
	if ( m_items.count(item) == 1 ) {
		m_items.erase(item);

		if (m_items.empty() ) {
			delete this;
		}
	} else {
		return;
	}
}

std::ostream& operator<<(std::ostream& out, const Order* order) {
	out << "--- Order #" << order->getID() << " ---" << std::endl;

	order->getItems();

	return out;
}

long Order::s_idGenerator;
