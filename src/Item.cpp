#include <iostream>
#include <string>
#include "Item.h"
#include "Category.h"
#include "Order.h"

Item::Item(const std::string name, double price, Category* category) :
	m_name(name), m_price(price), m_category(category) 
{
	m_category->addItem(this);
}

Item::~Item() {
	m_category->deleteItem(this);

	if ( !m_orders.empty() ) {
		auto it = m_orders.begin();

		while ( it != m_orders.end() ) {
			(*it)->deleteItem(this);
		}
	}

	std::cout << "Item \"" << m_name << "\" is deleted!\n";
}

std::string Item::getName() const {
	return m_name;
}

double Item::getPrice() const {
	return m_price;
}

std::string Item::getCategory() const {
	return m_category->getName();
}

void Item::addOrder(Order* order) {
	m_orders.insert(order);
}

void Item::removeOrder(Order* order) {
	m_orders.erase(order);
}

std::ostream& operator<<(std::ostream& out, const Item* item) {
	out << item->getName() << " " << item->getPrice() << " " << item->getCategory();

	return out;
}