#ifndef ITEM_H
#define ITEM_H

#include <iostream>
#include <string>
#include <set>

class Category;
class Order;

class Item {
private:
	std::string m_name;
	double m_price;
	Category* m_category;
	std::set<Order*> m_orders;

public:
	Item(const std::string name, double price, Category* category);

	~Item();

	std::string getName() const;

	double getPrice() const;

	std::string getCategory() const;

	void addOrder(Order* order);

	void removeOrder(Order* order);

};

std::ostream& operator<<(std::ostream& out, const Item* item);


#endif //ITEM_H