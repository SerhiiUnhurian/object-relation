#ifndef CUSTOMER_H
#define CUSTOMER_H

#include <iostream>
#include <set>

class Order;

class Customer {
private:
	std::string m_name;
	std::set<Order*> m_orders;

public:
	Customer(std::string name);

	~Customer();

	std::string getName() const;

	void getOrders() const;

	const std::set<Order*>& getAllOrders() const;

	void addOrder(Order* order);
	void deleteOrder(Order* order);
};

std::ostream& operator<<(std::ostream& out, const Customer* customer);

#endif // CUSTOMER_H