#include "Customer.h"
#include "Order.h"

Customer::Customer(std::string name) : m_name(name) 
{
}

Customer::~Customer() {
	if ( !m_orders.empty() ) {
		auto it = m_orders.begin();

		while ( it != m_orders.end() ) {
			delete *it;
			it++;
		}
	}

	std::cout << "Customer " << m_name << " is deleted!\n";
}

std::string Customer::getName() const {
	return m_name;
}

void Customer::getOrders() const {
	if ( !m_orders.empty() ) {
		auto it = m_orders.begin();

		while ( it != m_orders.end() ) {
			std::cout << *it << std::endl;
			it++;
		}
	}
}

const std::set<Order*>& Customer::getAllOrders() const {
	return m_orders;
}

void Customer::addOrder(Order* order) {
	m_orders.insert(order);	
}

void Customer::deleteOrder(Order* order) {
	m_orders.erase(order);
}

std::ostream& operator<<(std::ostream& out, const Customer* customer) {
	int size = customer->getAllOrders().size();
	std::string ending = size > 1 ? "s" : "";

	out << "Customer " << customer->getName() << " has "<< size << " order" << ending << std::endl;

	auto it = customer->getAllOrders().begin();

	while ( it != customer->getAllOrders().end() ) {
		out << "#" << (*it)->getID() << std::endl;
		it++;
	}

	return out;
}