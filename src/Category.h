#ifndef CATEGORY_H
#define CATEGORY_H

#include <string>
#include <iostream>
#include <set>

class Item;

class Category {
private:
	std::string m_name;
	std::set<Item*> m_items;
	long long m_quantity;

public:
	Category(const std::string name);

	~Category();

	std::string getName() const;
	
	long long getQuantity() const;

	void addItem(Item* item);

	void deleteItem(Item* item);

	void showItems() const;
};

std::ostream& operator<<(std::ostream& out, const Category* category);

#endif //CATEGORY_Hcd 