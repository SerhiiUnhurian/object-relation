#ifndef ORDER_H
#define ORDER_H

#include <map>

class Item;
class Customer;

class Order {
private:
	static long s_idGenerator;
	long m_id;
	Customer* m_owner;
	std::map<Item*, int> m_items;

public:
	Order(Customer* owner, Item* item);

	~Order();

	long getID() const;

	Customer* getOwner() const;

	void getItems() const;

	void addItem(Item* item, int quantity=1);

	void reduceItem(Item* item);

	void deleteItem(Item* item);
};

std::ostream& operator<<(std::ostream& out, const Order* order);

#endif // ORDER_H