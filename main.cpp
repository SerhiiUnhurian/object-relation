#include "Category.h"
#include "Item.h"
#include "Order.h"
#include "Customer.h"

#include <iostream>

int main() {
    Category* c1 = new Category("Utensils");

    Item* i1 = new Item("Broom", 7.99, c1);
    Item* i2 = new Item("Fork", 5.99, c1);


    Customer* cus1 = new Customer("Serhii");

    Order* o1 = new Order(cus1, i1);

    o1->addItem(i1);
    o1->addItem(i1, 2);
    o1->addItem(i1);
    o1->addItem(i2, 1);

    std::cout << o1 << std::endl;

    o1->reduceItem(i1);
    o1->reduceItem(i1);
    o1->reduceItem(i1);

    cus1->deleteOrder(o1);
    cus1->deleteOrder(o1);
    o1->reduceItem(i2);
    o1->reduceItem(i2);
    o1->reduceItem(i1);
    o1->deleteItem(i1);

    o1->deleteItem(i2);

    cus1->addOrder(o1);

    std::cout << cus1 << std::endl;

    delete cus1;
    delete o1;

    o1->addItem(i1, 2);
    o1->addItem(i1, 2);
    std::cout << o1 << std::endl;


	return 0;
}